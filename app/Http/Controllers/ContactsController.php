<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\User;
use Storage;
use Session;

class ContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$objContact=new Contact();
        $objUser=new User();
        $user=\Auth::user();
        $dataContacts=$user->contactsbyUser()->get()->all();
        return view("contacts.list_contacts")->with('contacts',$dataContacts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   

       $contact= (object) null; 
       return view("contacts.create")->with(['route'=>'save','title'=>'Crear Contacto','contact'=>$contact]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        
        try {
            $name_file=true;
            if(!empty($request->file)){
                $name_file=$this->saveImg($request->file);
            }
            if($name_file!=false){
                $user=\Auth::user();
                $request=(object) $request->all();
                $objContact= new Contact();
                $objContact->name=$request->name;
                $objContact->lastname=$request->lastname;
                $objContact->phone=$request->phone;
                $objContact->address=$request->address;
                $objContact->email=$request->email;
                $objContact->user_id=$user->id;
                $objContact->image=$name_file;
                if($objContact->save()){
                    \Log::info('Se ha creado satisfactoriamente el contacto: '.$request->email);
                    $contact= (object) null; 
                    Session::flash('mensaje', 'Contacto creado con exito!');
                }else{
                    Session::flash('mensaje', 'Error al Guardar Contacto!');
                }
                return redirect()->back()->with(['title'=>'Crear Contacto','contact'=>$contact]);
            }
        } catch (Exception $e) {
            \Log::error('Error al crear contacto LINE: '.$e->getLine().' FILE: '.$ex->getFile());
        }

    }

    public function saveImg($file){
      $name_file=time().'_'.$file->getClientOriginalName(); 
      if(Storage::disk('contactos')->put($name_file,file_get_contents($file->getRealPath()))){
        return $name_file;
      }else{
        return false;
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $objContact=new Contact();
        $id=intval($id);
        $dataContact=$objContact::find($id);
        return view("contacts.create")->with(['title'=>'Modificar Contacto','contact'=>$dataContact]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
          try {
            $objContact= new Contact();
            $id=intval($id);
            $objContact=$objContact::find($id);
            $objContact->name=$request->name;
            $objContact->lastname=$request->lastname;
            $objContact->phone=$request->phone;
            $objContact->email=$request->email;
            $objContact->address=$request->address;
            if($objContact->save()){
                $request->session()->flash('mensaje', '¡El usuario fue Modificado!');
                 \Log::info('Se ha modificado satisfactoriamente el contacto: '.$request->email);
            }else{
                $request->session()->flash('mensaje', '¡error!');
            }

            return redirect()->back()->with(['title'=>'Modificar Contacto','contact'=>$objContact]);

            //return view("contacts.create")
        
        } catch (Exception $e) {
             $request->session()->flash('mensaje', '¡error!');
            \Log::error('Error al crear contacto LINE: '.$e->getLine().' FILE: '.$ex->getFile());
        }

      
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
