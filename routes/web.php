<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//Route::resource('contacts','ContactsController');
Route::get('/createcontacts', 'ContactsController@create')->name('create')->middleware('auth');
Route::get('listContacts', 'ContactsController@index')->name('listcontact')->middleware('auth');
Route::get('editContact/{id}', 'ContactsController@edit')->name('editcontact')->middleware('auth');
Route::put('updateContact/{id}', 'ContactsController@update')->name('update')->middleware('auth');
Route::post('/savecontact', 'ContactsController@store')->name('save');
