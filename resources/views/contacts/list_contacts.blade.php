@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Listar Contacto</div>
                <div class="card-body">
                    {{-- table>(thead>tr>td>{Nombre})+tbody>tr>td>{Valor} --}}
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Email</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(!empty($contacts))
                            @foreach($contacts as $value)
                            <tr>
                                <td>{{$value->name.' '.$value->lastname}}</td>
                                <td>{{$value->email}}</td>
                                <td>
                                    <a href="editContact/{{$value->id}}" class="btn btn-success">Modificar</a>
                                    <button class="btn btn-danger">Eliminar</button>
                                </td>
                            </tr>
                            @endforeach
                            @else
                            <tr colspan="3"><td>No se encontraron registros</td></tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection