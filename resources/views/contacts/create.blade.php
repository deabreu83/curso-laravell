@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{$title}}</div>
                <div class="card-body">
                    
                    @if(session()->has('mensaje'))
                    <div class="alert alert-primary" role="alert">
                      {{session('mensaje')}}
                    </div>
                    @endif

                    <form method="POST" action="{{!empty($contact->id)?url('updateContact/'.$contact->id):route('save')}}" enctype="multipart/form-data" >
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Nombre</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{isset($contact->name)?$contact->name:""}}" required >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Apellido</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="lastname" value="{{isset($contact->lastname)?$contact->lastname:""}}" required >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Phone</label>

                            <div class="col-md-6">
                                <input id="name" type="number" class="form-control" name="phone" value="{{isset($contact->phone)?$contact->phone:""}}" required >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Email</label>

                            <div class="col-md-6">
                                <input id="name" type="email" class="form-control" name="email" value="{{isset($contact->email)?$contact->email:""}}" required >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="file" class="col-md-4 col-form-label text-md-right">Foto</label>

                            <div class="col-md-6">
                                <input id="file" type="file" class="form-control" name="file"  >
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Address</label>
                            <div class="col-md-6">
                                {{-- textarea[cols="3"][rows=2].form-control#address[placeholder="Ingresa tu direccion"] --}}
                                <textarea name="address" id="address" cols="3" rows="5" class="form-control" placeholder="Ingresa tu direccion">
                                    {{isset($contact->address)?$contact->address:""}}
                                </textarea>
                            </div>
                        </div>
                        <a class="btn btn-danger" href="{{route('home')}}">Atras</a>
                        @if(isset($contact->id))
                            <input type="hidden" name="_method" value="PUT">
                        @endif
                        <button class="btn btn-success" type="submit">Guardar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection